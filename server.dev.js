const express = require('express');
const path = require('path');
const ip = require('ip');
const app = express();

const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('webpack.config')({
  name: 'dev'
});

const compiler = webpack(webpackConfig);

app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath,
  stats: {
    colors: true,
  },
  historyApiFallback: true,
}));

// enable hot for dev middleware
app.use(webpackHotMiddleware(compiler, {
  reload: true,
}));

// allow routing without path error by directing to index output
app.get('*', function (req, res) {
  const outputFS = compiler.outputFileSystem;
  const index = path.join(webpackConfig.output.path, 'index.html');
  const html = outputFS.readFileSync(index);
  res.end(html);
});

const serverLocal = app.listen(3000, "127.0.0.1", function() {
  const host = serverLocal.address().address;
  const port = serverLocal.address().port;
  console.log('[App] listening on localhost at http://%s:%s', host, port);
});

const serverNetwork = app.listen(3000, ip.address(), function() {
  const host = serverNetwork.address().address;
  const port = serverNetwork.address().port;
  console.log('[App] listening on network at http://%s:%s', host, port);
});
