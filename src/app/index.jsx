import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import 'font-awesome/css/font-awesome.css';
// font awesome icons

import 'whatwg-fetch';
// api calls

import './style.scss';
import Home from 'scenes/Home';
import Example from 'scenes/Example';

class App extends React.Component {
  render() {
    return (  /* wrapper/container */
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={ Home } />
          <Route exact path="/example" component={ Example } />
          <Redirect from="/allenduy.me" to="/" />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
