import React from 'react';
import { Link } from 'react-router-dom';

import './style.scss';

class Button extends React.PureComponent {
  render() {
    return this.props.to ? (
      <Link to={ this.props.to }>
        <span className="button">{ this.props.label }</span>
      </Link>
    ) : (
      <a href={ this.props.href }>
        <span className="button">{ this.props.label }</span>
      </a>
    );
  }
}

export default Button;