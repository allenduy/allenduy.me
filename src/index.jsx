import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import App from './app';
import { reducer } from './app/reducer';

const store = createStore(reducer);
const rootElement = document.getElementById('root');

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={ store }>
        <Component />
      </Provider>
    </AppContainer>,
    rootElement
  );
}

render(App);

if (module.hot) {
  module.hot.accept('./app', () => { render(App) });
}

