import React from 'react';

import './style.scss';
import Button from 'components/Button';

class Example extends React.Component {
  render() {
    return (
      <div styleName="container">
        <div className="content">
          <span className="title">
            aw yiss.
            <span className="caption">and some caption text.</span>
          </span>
          <Button to="/" label="go home" />
        </div>
        <div className="background"></div>
      </div>
    );
  }
}

export default Example;
