import React from 'react';

import './style.scss';
import Button from 'components/Button';

class Home extends React.Component {
  render() {
    return (
      <div styleName="container">
        <div className="content">
          <span className="title">
            allenduy
            <span className="caption">full site coming soon</span>
          </span>
          <Button href="https://goo.gl/forms/UnYyyIQbC84Gdn6q2" label="commission form" />
        </div>
        <div className="background"></div>
      </div>
    );
  }
}

export default Home;
