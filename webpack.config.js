const merge = require('webpack-merge');
const path = require('path');
const context = path.resolve(__dirname, 'src');
const webpack = require('webpack');

const plugin = {
  extractText: require('extract-text-webpack-plugin'),
  html: require('html-webpack-plugin'),
  reactContainer: require('stateful-react-container-webpack-plugin'),
  template: require('html-webpack-template'),
};


/******************************************************************************
 *
 * Empty Configuration
 *
 *****************************************************************************/


const Config = {
  empty: {
    context: null,
    entry: [],
    output: {
      // path: output directory
      // publicPath: HMR browser load directory
      // fileName: output name
    },
    module: {
      rules: [
        // CSS loaders
        // style: injects <style> tag to dom
        // css: resolves @import and url() as import and require()
        //    (default localIdentName: [hash:base64])
        // postcss: plugins to make your styling easier
        //    (precss: enable use of sass-like markup in css)
        //    (autoprefixer: auto add vendor prefixes; -webkit-, -moz-, etc.)

        // JS loaders
        // react-css-modules (babel-plugin-react-css-modules)
        //    loads styleName (default scopeName:
        //    [path]___[name]__[local]___[hash:base64:5])
        //    must match css-loader's localIdentName+context

        // URL loader (for icon fonts (font-awesome, icomoon))
      ]
    },
    resolve: {},
    plugins: [],
    devtool: null,
    stats: ''
  },

  base: function(repo) {
    return {
      context,

      entry: [
        // root injection of application
        './index.jsx',
      ],

      output: {
        publicPath: repo ? `/${repo}/` : '/',
        filename: 'app.js',
      },

      module: {
        rules: [
          {
            test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url-loader',
            options: {
              limit: 10000,
            },
          }
        ],
      },

      resolve: {
        modules: [
          context,
          path.join(__dirname, 'node_modules'),
        ],
        extensions: ['.js', '.json', '.jsx'],
      },

      plugins: [
        new plugin.html({
          inject: false,
          template: plugin.template,
          title: 'allendewy\'s official webpage',
          mobile: true,
          baseHref: repo ? `/${repo}/` : '/',
        }),
        new plugin.reactContainer({
          id: 'root',
          noState: true,
        }),
      ],
    };
  }
};


/******************************************************************************
 *
 * Production Configuration
 *
 *****************************************************************************/


Config.prod = function(repo) {
  return merge([
    {
      output: {
        path: path.join(__dirname, 'www/')
      },

      module: {
        rules: [
          {
            test: /\.(sass|scss)$/,
            use: plugin.extractText.extract({
              fallback: 'style-loader',
              use: [
                {
                  loader: 'css-loader',
                  options: {
                    minimize: true,
                    modules: true,
                    importLoaders: 1,
                  },
                },
                {
                  loader: 'postcss-loader',
                  options: {
                    plugins: () => [
                      require('precss'),
                      require('autoprefixer'),
                    ],
                  },
                },
              ]
            }),
          },
          {
            test: /\.css$/,
            use: plugin.extractText.extract({
              fallback: 'style-loader',
              use: 'css-loader',
            }),
          },
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              presets: [
                ['env', { modules: false }],
                'react'
              ],
              plugins: [
                [
                  'react-css-modules',
                  {
                    context,
                    filetypes: { '.scss': 'postcss-scss' },
                    generateScopedName: '[hash:base64]',
                  }
                ],
              ],
            },
          },
        ],
      },

      plugins: [
        new plugin.extractText('styles.css')
      ],
    },

    Config.base(repo),

    {
      plugins: [
        new webpack.DefinePlugin({
          'process.env': {
            // production build of react
            NODE_ENV: JSON.stringify('production'),
          },
        }),
        new webpack.optimize.UglifyJsPlugin({
          beautify: false,
          mangle: {
            screw_ie8: true,
          },
          compress: {
            screw_ie8: true,
            warnings: false,
            comparisons: false,
          },
          output: {
            comments: false,
          },
          // sourceMap: options.devtool && (options.devtool.indexOf('sourcemap') >= 0 || options.devtool.indexOf('source-map') >= 0),
        })
      ]
    }
  ]);
};


/******************************************************************************
 *
 * Development Configuration
 *
 *****************************************************************************/


Config.dev = function() {
  return merge([
    {
      entry: [
        // enable hot reloading
        'react-hot-loader/patch',
        'webpack-hot-middleware/client',
      ],

      output: {
        path: path.join(__dirname, 'build/'),
      },

      plugins: [
        new webpack.HotModuleReplacementPlugin(),
      ],

      module: {
        rules: [
          {
            test: /\.(sass|scss)$/,
            use: [
              'style-loader',
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  importLoaders: 1,
                  localIdentName: '[path]___[name]__[local]___[hash:base64:5]',
                },
              },
              {
                loader: 'postcss-loader',
                options: {
                  plugins: () => [
                    require('precss'),
                    require('autoprefixer'),
                  ],
                },
              },
            ],
          },
          {
            test: /\.css$/,
            use: [
              'style-loader',
              'css-loader',
            ],
          },
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              presets: [
                ['env', { modules: false }],
                'react'
              ],
              plugins: [
                'react-hot-loader/babel',
                [
                  'react-css-modules',
                  {
                    context,
                    filetypes: { '.scss': 'postcss-scss' },
                    webpackHotModuleReloading: true,
                  }
                ],
              ],
            },
          },
        ],
      },

      devtool: 'eval-source-map',

      stats: 'detailed',
    },

    Config.base()
  ]);
};


/******************************************************************************
 *
 * Exports
 *
 *****************************************************************************/


module.exports = function(env) {
  switch (env.name) {

    case 'dev':
      return Config.dev();

    case 'prod':
      return Config.prod();

    case 'pages':
      return env.name ? Config.prod(env.repo) : null;

    default:
      return null;

  }
};
